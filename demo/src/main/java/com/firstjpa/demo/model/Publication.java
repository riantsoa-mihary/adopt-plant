/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import com.firstjpa.demo.utilitaire.Constante;
import com.firstjpa.demo.utilitaire.Heure;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author
 */

@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "puboffre")
public class Publication implements Serializable{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String id;
    protected String idutilisateur,sujet,descsujet,image,idendroit;
    @Column(insertable = false)
    protected Date datepub;
    protected Date daterdv;
    protected int nombre, etat, niveaujardin;
    protected double altitude,heurerdv;

    @ManyToOne @JoinColumn(name = "idutilisateur" , insertable = false, updatable = false)
    protected Utilisateur utilisateur;
    
    @ManyToOne @JoinColumn(name = "idendroit" , insertable = false, updatable = false)
    protected Endroit endroit;
    
    @OneToMany @JoinColumn(name = "idpuboffre")
    protected List<Reservation> reservation;
    
    
    public void init(){
        this.etat = Constante.STATE_CREATED;
        this.setAltitude(0.00); 
        this.nombre = 0;
        this.sujet = "";
        this.setDescsujet("");
        this.niveaujardin = Constante.INIT_NIVEAU_JARDIN;
        this.idendroit = "";
        this.heurerdv = 0;
    }
    public Publication() {
       this.init();
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdutilisateur() {
        return idutilisateur;
    }

    public void setIdutilisateur(String idutilisateur) {
        this.idutilisateur = idutilisateur;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet)  {
        this.sujet = sujet;
    }

    public String getDescsujet() {
        return descsujet;
    }

    public void setDescsujet(String descsujet) {
        if(descsujet == null)
            this.descsujet = Constante.STR_VIDE;
        else {
            this.descsujet = descsujet;
        }
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdendroit() {
        return idendroit;
    }

    public void setIdendroit(String idendroit) {
        this.idendroit = idendroit;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public Date getDaterdv() {
        return daterdv;
    }

    public void setDaterdv(Date daterdv) {
        this.daterdv = daterdv;
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public double getHeurerdv() {
        return heurerdv;
    }

    public void setHeurerdv(double heurerdv) {
        this.heurerdv = new Heure(heurerdv).getValue();
    }

    public int getNiveaujardin() {
        return niveaujardin;
    }

    public void setNiveaujardin(int niveaujardin) {
        this.niveaujardin = Constante.niveau_jardin_valid(niveaujardin);
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        if(altitude < 0){
            this.altitude = Constante.ZERO.doubleValue();
        }
        else {
            this.altitude = altitude;
        }
           
    }

    public Endroit getEndroit() {
        return endroit;
    }

    public void setEndroit(Endroit endroit) {
        this.endroit = endroit;
    }

    public List<Reservation> getReservation() {
        return reservation;
    }

    public void setReservation(List<Reservation> reservation) {
        this.reservation = reservation;
    }

    @Transient
    protected double diffAltitude;
    void setDiffAltitude(double altitude) {
        double diff = this.getAltitude() - altitude;
        if(diff < 0)
            diff = diff * (-1);
        this.diffAltitude = diff;
    }

    double getDiffAltitude() {
        return this.diffAltitude;
    }

    @Override
    public String toString() {
        return "Publication{" + "id=" + id + ", idutilisateur=" + idutilisateur + ", sujet=" + sujet + ", descsujet=" + descsujet + ", image=" + image + ", idendroit=" + idendroit + ", datepub=" + datepub + ", daterdv=" + daterdv + ", nombre=" + nombre + ", etat=" + etat + ", niveaujardin=" + niveaujardin + ", altitude=" + altitude + ", heurerdv=" + heurerdv + ", utilisateur=" + utilisateur + ", endroit=" + endroit + ", reservation=" + reservation + ", diffAltitude=" + diffAltitude + '}';
    }/*
    public void controlBeforeInsert() throws GestionException{
        ControlPublicationInsert control = new ControlPublicationInsert(this);
        if(control.hasException()){
            throw control.getException();
        }
    }*/
    
}
