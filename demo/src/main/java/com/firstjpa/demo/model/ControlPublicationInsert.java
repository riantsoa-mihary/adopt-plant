/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import com.firstjpa.demo.utilitaire.Constante;
import java.util.Date;
import org.springframework.http.HttpStatus;

/**
 *
 * @author
 */
public class ControlPublicationInsert {
    private Publication publication;
    private GestionException exception;

    public ControlPublicationInsert(Publication publication) {
        this.setPublication(publication);
        this.setException(new GestionException(HttpStatus.BAD_REQUEST));
        this.control();
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public GestionException getException() {
        return exception;
    }

    public void setException(GestionException exception) {
        this.exception = exception;
    }

    
    
    private void control(){
        this.setSujet();
        this.getPublication().setDescsujet(this.getPublication().getDescsujet());
        this.setNombre();
        this.getPublication().setAltitude(this.getPublication().getAltitude());
        this.setIdendroit();
        this.setDaterdv();
        this.getPublication().setHeurerdv(this.getPublication().getHeurerdv());
    }
    public void setSujet(){
        String sujet = this.getPublication().getSujet();
        if(sujet == null)
            this.exception.add("sujet", Constante.getChampObli("sujet"));
        else {
            String s_trim = sujet.trim();
            if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
                this.exception.add("sujet", Constante.getChampObli("sujet"));
            }
        }
    }
    public void setNombre() {
        int nombre = this.getPublication().getNombre();
        if(nombre == 0)
            this.exception.add("nombre", Constante.getChampObli("nombre de plante"));

    }
    
    public void setDaterdv(){
        Date daterdv = this.getPublication().getDaterdv();
        if(daterdv == null)
            this.exception.add("daterdv", Constante.getChampObli("Date de rendez-vous"));
    }
    
    public void setIdendroit() {
        String idendroit = this.getPublication().getIdendroit();
        if(idendroit == null)
            this.exception.add("idendroit", Constante.getChampObli("Endroit"));
        else {
            String s_trim = idendroit.trim();
            if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
                this.exception.add("idendroit", Constante.getChampObli("Endroit"));
            }
        }
    }
    public boolean hasException() {
        return this.getException().hasMessage();
    }
    
}
