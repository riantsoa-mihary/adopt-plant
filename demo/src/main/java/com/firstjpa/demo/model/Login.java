/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import com.firstjpa.demo.utilitaire.Constante;
import javax.persistence.Transient;
import org.springframework.http.HttpStatus;

/**
 *
 * @author
 */

public class Login{
    
    @Transient
    private GestionException exception;
    
    protected String mdp;
    protected String email;

    
    public Login(){
        super();
        this.setException(new GestionException(HttpStatus.BAD_REQUEST));
    }

    public void setEmail(String email){
        if(email == null)
            this.exception.add("email", Constante.getChampObli("email"));
        else {
            String s_trim = email.trim();
            if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
                this.exception.add("email", Constante.getChampObli("email"));
            }
            else this.email = email;
        }
    }   
    public void setMdp(String mdp){
        if(mdp == null)
            this.exception.add("mdp", Constante.getChampObli("Mot de Passe"));
        else {
            String s_trim = mdp.trim();
            if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
                this.exception.add("mdp", Constante.getChampObli("Mot de Passe"));
            }
            else this.mdp = mdp;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getMdp() {
        return mdp;
    }

    public GestionException getException() {
        return exception;
    }

    public void setException(GestionException exception) {
        this.exception = exception;
    }
    public void controlValeurLogin() throws GestionException{
        this.setEmail(email);
        this.setMdp(mdp);
        if(this.getException().hasMessage() ) throw this.getException();
    }
    //public void controlValeurInsert() throws Exception{ + niveau jardin, +superficie jardin

    @Override
    public String toString() {
        return "Login{" + "exception=" + exception + ", mdp=" + mdp + ", email=" + email + '}';
    }
    
}
