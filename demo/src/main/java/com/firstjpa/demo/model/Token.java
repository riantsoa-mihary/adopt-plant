/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import com.firstjpa.demo.utilitaire.Constante;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import org.springframework.http.HttpStatus;

/**
 *
 * @author
 */
@NamedNativeQuery(name = "findByToken", 
        query = " select * from tokenvalide t where t.token like :token ",
        resultClass = Token.class
        )
@Entity
@Table(name = "Token")
public class Token {


    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String token;
    
    private String idutilisateur;
    
    @ManyToOne @JoinColumn(name = "idutilisateur" , insertable = false, updatable = false)
    protected Utilisateur utilisateur;

    public Token() {
    }
    public Token(String idutilisateur) {
        this.setIdutilisateur(idutilisateur);
    }
    

    public String getToken() {
        return token;
    }

    public void setToken(String token) throws GestionException {
        GestionException exception = new GestionException(HttpStatus.BAD_REQUEST);
        if(token == null){
            exception.add("token", Constante.TOKEN_VIDE_MESSAGE);
            throw exception;
        }
        else {
            String s_trim = token.trim();
            if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
                exception.add("token", Constante.TOKEN_VIDE_MESSAGE);
                throw exception;
            }
            else this.token = token;
        }
    }

    public String getIdutilisateur() {
        return idutilisateur;
    }

    public void setIdutilisateur(String idutilisateur) {
        this.idutilisateur = idutilisateur;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
    
    
}
