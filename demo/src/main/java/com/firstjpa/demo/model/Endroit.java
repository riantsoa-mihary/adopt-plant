/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author
 */
@Entity 
@Table(name = "endroit")
public class Endroit implements Serializable{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String idville, endroit, descendroit;
    private double longitude, latitude;
    
    @ManyToOne @JoinColumn(name = "idville" , insertable = false, updatable = false)
    private Ville ville;
    
    public Endroit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdville() {
        return idville;
    }

    public void setIdville(String idville) {
        this.idville = idville;
    }

    public String getEndroit() {
        return endroit;
    }

    public void setEndroit(String endroit) {
        this.endroit = endroit;
    }

    public String getDescendroit() {
        return descendroit;
    }

    public void setDescendroit(String descendroit) {
        this.descendroit = descendroit;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }
    
    
}
