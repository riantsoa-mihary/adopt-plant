/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 *
 * @author
 */
@NamedNativeQuery(name = "findByLogin", 
        query = " select * from utilisateur u where u.email like :email and u.mdp like (select md5(:mdp))  ",
        resultClass = Utilisateur.class
        )
@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable{
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String id;

    protected String nom, photos;
    
    
    protected int niveaujardin, superficiejardin;
    
    public Utilisateur() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public int getNiveaujardin() {
        return niveaujardin;
    }

    public void setNiveaujardin(int niveaujardin) {
        this.niveaujardin = niveaujardin;
    }

    public int getSuperficiejardin() {
        return superficiejardin;
    }

    public void setSuperficiejardin(int superficiejardin) {
        this.superficiejardin = superficiejardin;
    }
/*
    @Override
    public String toString() {
        return "Utilisateur{" + "id=" + id + ", nom=" + nom + ", mdp=" + mdp + ", photos=" + photos + ", niveaujardin=" + niveaujardin + ", superficiejardin=" + superficiejardin + '}';
    }

    */
    
}
