/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.firstjpa.demo.utilitaire.Constante;
import com.firstjpa.demo.dao.PublicationDao;
import com.firstjpa.demo.utilitaire.Heure;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author
 */
public class Recommendation {
    private String sujet; 
    private String date1; 
    private String date2; 
    private double heure1; 
    private double heure2; 
    private int niveaujardin; 
    private double altitude;
    private String ville;
    private int margeAltitude;
    private boolean hasAltitude;

    public Recommendation(String sujet, String date1, String date2, double heure1, double heure2, int niveaujardin, double altitude, int margeAltitude, String ville) {
        this.setHasAltitude(true);
        this.setSujet(sujet);
        this.setDate1(date1);
        this.setDate2(date2);
        this.setHeure1(heure1);
        this.setHeure2(heure2);
        this.setNiveaujardin(niveaujardin);
        this.setAltitude(altitude);
        this.setVille(ville);
        this.setMargeAltitude(margeAltitude);
    }/*
    public Publication getMinDiffAltiude(List<Publication> liste){
        Publication min = null;
        for(Publication p : liste){
            if(p.getDejaPris() == false){
                min = p;
                break;
            }
        }
        for(Publication p : liste){
            if( p.getDejaPris() == false && p.getDiffAltitude() <= min.getDiffAltitude()){
                min = p;
            }
        }
        return min;
    }
    public List<Publication> rapprocher(List<Publication> liste){
        List<Publication> result = new ArrayList<Publication>();
        
        for(Publication p : liste){
            p.setDiffAltitude(this.getAltitude());
            p.setDejaPris(false);
        }
        Publication min = null;
        for(int i = 0 ; i < liste.size(); i++){
            min = this.getMinDiffAltiude(liste);
            min.setDejaPris(true);
            result.add(min);
        }
        return result;
    }
    
*/
    
    private int find_Future_Indice(List<Publication> liste,double chiffre_quelconque){
        int taille = liste.size();

        int depart = 0, 
            arrive = 0,
            initial = 0, // depart
            moyenne = 0, // ancien_moyenne
            ancienne_moyenne = 0,
            indice = 0;
        double val = Double.MIN_VALUE; // temporary to list value

        /* initializing */
        initial = 0;
        depart = initial;  arrive = taille;
        /* -- initializing */

        if(taille > 0){
            while( val != chiffre_quelconque) {
                ancienne_moyenne = moyenne;
                moyenne = (arrive - depart) /2;    moyenne += initial;
                val = liste.get(moyenne).getDiffAltitude();
                if(moyenne == ancienne_moyenne){
                    indice = 0;
                    if(chiffre_quelconque < val){
                        indice = -1;
                    }
                    else if(chiffre_quelconque > val){
                        indice = 1;
                    }
                    moyenne += indice;
                    break;
                }
                else {
                    if(chiffre_quelconque < val) {
                        arrive = moyenne; 
                    }
                    else if(chiffre_quelconque > val){
                        depart = moyenne;
                        initial = depart;
                    }
                }
            }
        }
        if( moyenne < 0 ) moyenne = 0;
        return moyenne;
    }
    
    public List<Publication> trier(List<Publication> tab){
        int right_indice;
        List<Publication>  chiffre = new ArrayList();
        int taille = tab.size();
        Publication temp ;
        for(int i=0; i< taille; i++) {
            temp = tab.get(i);
            right_indice = find_Future_Indice(chiffre, temp.getDiffAltitude());
            chiffre.add(right_indice, temp);
        }
        return chiffre;
    }
    public List<Publication> rapprocher(List<Publication> liste){        
        for(Publication p : liste){
            p.setDiffAltitude(this.getAltitude());
        }
        return this.trier(liste);
    }
    
    private RecommendationQuery getRecoQuery() throws ParseException {
        RecommendationQuery query = new RecommendationQuery("Publication","p");
        query.add("sujet",this.getSujet()," and sujet like :sujet ");
        query.add("ville",this.getVille()," and p.endroit.ville.id like :ville ");
        query.add("niveaujardin",this.getNiveaujardin()," and niveaujardin <= :niveaujardin ");
        if(this.HasAltitude()){
            query.add("altitudemin",this.getMinAltitude()," and altitude >= :altitudemin ");
            query.add("altitudemax",this.getMaxAltitude()," and altitude <= :altitudemax ");
        }
        DateFormat df = new StdDateFormat();
        if(this.getDate1() == null) {
            query.addCondition(" and daterdv >= now() ");
        }
        else {
            query.add("daterdvmin",df.parse(this.getDate1())," and daterdv >= :daterdvmin ");
        }
        if(this.getDate2() != null) {
            query.add("daterdvmax",df.parse(this.getDate2())," and daterdv <= :daterdvmax ");
        }
        query.add("heurerdvmin",this.getHeure1()," and heurerdv >= :heurerdvmin ");
        if(this.getDate2() != null && this.getHeure2() > 0){
            query.add("heurerdvmax",this.getHeure2()," and heurerdv <= :heurerdvmax ");
        }
        return query;
    }
    public List<Publication> getRecommendation(PublicationDao publicationDao) throws ParseException{
        List<Publication> resultat = null;
        List<Publication> liste = publicationDao.findPublication(this.getRecoQuery());
        resultat = this.rapprocher(liste);
        return resultat;
    }

    public double getMinAltitude(){
        return this.altitude - this.margeAltitude;
    }
    
    public double getMaxAltitude(){
        return this.altitude + this.margeAltitude;
    }
    public String getSujet() {
        return "%"+sujet+"%";
    }

    public void setSujet(String s) {
        String s_trim = s.trim();
        if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
            this.sujet = Constante.STR_VIDE;
        }
        else this.sujet = s_trim;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        String s_trim = date1.trim();
        if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
            this.date1 = null;
        }
        else this.date1 = s_trim;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        String s_trim = date2.trim();
        if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
            this.date2 = null;
        }
        else this.date2 = s_trim;
    }

    public double getHeure1() {
        return heure1;
    }

    
    public void setHeure1(double heure1) {
        this.heure1 = new Heure(heure1).getValue();
    }

    public double getHeure2() {
        return heure2;
    }

    public void setHeure2(double heure2) {
        this.heure2 = new Heure(heure2).getValue();
    }

    public int getNiveaujardin() {
        return niveaujardin;
    }

    public void setNiveaujardin(int niveaujardin) {
        if(niveaujardin ==  Constante.ZERO.intValue()){
            this.niveaujardin = Constante.INIT_NIVEAU_JARDIN;
        }
        else this.niveaujardin = niveaujardin;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        if(altitude ==  Constante.ZERO.doubleValue()){
            this.setHasAltitude(false);
        }
        this.altitude = altitude;
    }

    public String getVille() {
        return "%"+ville+"%";
    }

    public void setVille(String ville) {
        String s_trim = ville.trim();
        if(s_trim.equalsIgnoreCase(Constante.NULL) || 
                s_trim.equalsIgnoreCase(Constante.STR_VIDE)){
            this.ville = Constante.STR_VIDE;
        }
        else this.ville = s_trim;
    }

    public int getMargeAltitude() {
        return margeAltitude;
    }

    public void setMargeAltitude(int margeAltitude) {
        this.margeAltitude = margeAltitude;
    }

    public boolean HasAltitude() {
        return hasAltitude;
    }

    public void setHasAltitude(boolean hasAltitude) {
        this.hasAltitude = hasAltitude;
    }

    @Override
    public String toString() {
        return "Recommendation{" + "sujet=" + sujet + ", date1=" + date1 + ", date2=" + date2 + ", heure1=" + heure1 + ", heure2=" + heure2 + ", niveaujardin=" + niveaujardin + ", altitude=" + altitude + ", ville=" + ville + ", margeAltitude=" + margeAltitude + ", hasAltitude=" + hasAltitude + '}';
    }

    
    
}
