/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 *
 * @author
 */
public class GestionException extends Exception{
    private HttpStatus status;
    private boolean hasMessage;
    private List<KeyValue> keyValues;
    
    public GestionException(HttpStatus status, String message){
        super(message);
        this.setStatus(status);
        this.setHasMessageOff();
        this.setKeyValues(new ArrayList<>());
        this.add("exception", message);
    }
    public GestionException(HttpStatus status){
        this.setStatus(status);
        this.setHasMessageOff();
        this.setKeyValues(new ArrayList<>());
    }
    public void add(String key, String value){
        boolean in = false;
        for(KeyValue kv : this.getKeyValues()){
            if(kv.getKey().equalsIgnoreCase(key)) {
                kv.setValue(value);
                in = true;
                break;
            }
        }
        if(in == false) this.getKeyValues().add(new KeyValue(key, value));
        this.setHasMessageOn();
    }
    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        String msg = "";
        boolean notFirst = false;
        for(KeyValue kv : this.getKeyValues()){
            if(notFirst) {
                msg += ",";
            }
            else {
                notFirst = true;
            }
            msg += "\""+kv.getKey()+"\":\""+kv.getValue()+"\"";
        }
        String finalMessage = "[{"+msg+"}]";
        return finalMessage;
    }

    public boolean hasMessage() {
        return hasMessage;
    }

    public void setHasMessageOn() {
        this.hasMessage = true;
    }
    public void setHasMessageOff() {
        this.hasMessage = false;
    }

    public List<KeyValue> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(List<KeyValue> keyValues) {
        this.keyValues = keyValues;
    }
    
    
}
