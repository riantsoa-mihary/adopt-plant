/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author
 */
public class RecommendationQuery {
    private String table;
    private String alias;
    private String condition;
    private List<KeyValue> values;

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public void setTable(String table) {
        this.table = table;
    }
    private String getTable(){
        return " select "+this.alias+" from "+this.table+" "+this.alias+" " ;
    }

    public String getQuery() {
        return this.getTable() + this.condition;
    }

    public List<KeyValue> getValues() {
        return values;
    }

    public void setValues(List<KeyValue> kv) {
        this.values = kv;
    }

    public RecommendationQuery(String table, String alias) {
        this.init(table, alias);
    }
    private void init(String table, String alias) {
        this.setValues(new ArrayList<>());
        this.setTable(table);
        this.setAlias(alias);
        this.condition = " where 1 = 1 ";
    }

    void add(String key, Object val, String condition) {
        this.getValues().add(new KeyValue(key, val));
        this.addCondition(condition);
    }

    public void addCondition(String condition) {
        this.condition += condition;
    }

}
