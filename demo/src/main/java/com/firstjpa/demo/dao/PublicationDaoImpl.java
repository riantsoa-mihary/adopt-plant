/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.dao;

import com.firstjpa.demo.model.KeyValue;
import com.firstjpa.demo.model.Publication;
import com.firstjpa.demo.model.RecommendationQuery;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author
 */
@Repository
@Transactional
public class PublicationDaoImpl implements PublicationDao{

    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public List<Publication> findPublication(RecommendationQuery query) {
        Query q =  entityManager.createQuery(query.getQuery(),Publication.class);
        for(KeyValue kv : query.getValues()){
             q.setParameter(kv.getKey(), kv.getValue());
        }
        return q.getResultList();
    }
    
}
