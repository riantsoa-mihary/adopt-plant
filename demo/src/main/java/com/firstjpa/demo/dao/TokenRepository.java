/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.dao;

import com.firstjpa.demo.model.Token;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author
 */
public interface TokenRepository extends JpaRepository<Token, String>{
    @Query(nativeQuery = true, name = "findByToken")
    public List<Token> findByToken(@Param("token") String token);
}
