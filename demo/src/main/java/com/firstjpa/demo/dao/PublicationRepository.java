/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.dao;

import com.firstjpa.demo.model.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author
 */
public interface PublicationRepository extends JpaRepository<Publication, String>{
 
}
