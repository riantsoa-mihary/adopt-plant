/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.dao;

import com.firstjpa.demo.model.Utilisateur;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author
 */
public interface UtilisateurRepository extends JpaRepository<Utilisateur, String>{
    @Query(nativeQuery = true, name = "findByLogin")
    public List<Utilisateur> findByLogin(@Param("email") String email, @Param("mdp") String mdp);
    
}
