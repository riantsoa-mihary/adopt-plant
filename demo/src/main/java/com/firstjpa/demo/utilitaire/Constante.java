/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.utilitaire;

/**
 *
 * @author
 */
public class Constante {
    public static String CHAMP_OBLIGATOIRE = "Champ obligatoire : ";
    public static String EXCEPTION = "Exception";
    public static double HEURE_MAX = 24;
    public static int INIT_NIVEAU_JARDIN = 1;
    public static double MINUTE_MAX = 60;
    public static int[] NIVEAU_JARDIN = new int[]{1,11};
    public static String NULL = "null";
    public static double SECONDE_MAX = 60;
    public static String STR_VIDE = "";
    public static String UPLOAD_DIR = "uploads";
    public static Number ZERO = 0;
    public static int STATE_CREATED = 1 ;
    public static String TOKEN_VIDE_MESSAGE = "Veuillez-vous connecter";
    
    public static int niveau_jardin_valid(int niveau_jardin) {
        int retour = Constante.INIT_NIVEAU_JARDIN;
        for(int val : Constante.NIVEAU_JARDIN) {
            if( val == niveau_jardin) {
                retour = niveau_jardin;
                break;
            }
        }
        return retour;
    }
    
    public static String getChampObli(String champ){
        return CHAMP_OBLIGATOIRE + champ;
    }
}
