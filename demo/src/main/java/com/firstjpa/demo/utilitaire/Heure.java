/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.utilitaire;


/**
 *
 * @author
 */
public class Heure {
    private double value;

    public Heure(double value) {
        this.setValue(value);
    }

    public double getValue() {
        return value;
    }

    private double verifierHeure(double heure1) {
        double heure;
        if(heure1 < this.heureMin())
            heure = this.heureMin();
        else if(heure1 > this.heureMax())
            heure = this.heureMax();
        else
            heure = heure1;
        return heure;
    }
    
    private void setValue(double value) {
        this.value = this.verifierHeure(value);
    }
    
    public double heureEnSeconde(){
        return this.heureEnSeconde(this.value);
    }
    
    private double heureEnSeconde(double value){
        return value * Constante.MINUTE_MAX * Constante.SECONDE_MAX;
    }
    
    public double heureMax(){
        return this.heureEnSeconde(Constante.HEURE_MAX) -1;
    }
    
    public double heureMin(){
        return this.heureEnSeconde(0.0) ;
    }
    
}
