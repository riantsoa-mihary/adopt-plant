/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo;

import com.firstjpa.demo.model.GestionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.firstjpa.demo.model.Publication;
import com.firstjpa.demo.service.PublicationService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 *
 * @author
 */
@RestController
public class PublicationController {
    
    
    @Autowired
    PublicationService publicationService;

    
    @RequestMapping(
            value = "/publication", 
            method = RequestMethod.POST)
    public ResponseEntity<Publication> create(@RequestHeader String token, @RequestBody Publication publication ) throws GestionException{
        try {
            Publication create = publicationService.createPublication(publication, token);
            return new ResponseEntity<>(create, HttpStatus.OK);
        } catch (GestionException e) {
            throw e;
        } catch (Exception e){
            GestionException ex =new GestionException(HttpStatus.BAD_REQUEST,e.getMessage());
            throw ex;
        }
    }
    
    @RequestMapping(
            value = "/publication/{sujet}/{ville}/{date1}/{date2}/{heure1}/{heure2}/{niveaujardin}/{altitude}/{margealtitude}",
            method = RequestMethod.GET)
    public ResponseEntity<List<Publication>> listePub (
            @PathVariable String sujet,
            @PathVariable String ville,
            @PathVariable String date1,
            @PathVariable String date2,
            @PathVariable double heure1,
            @PathVariable double heure2,
            @PathVariable int niveaujardin,
            @PathVariable double altitude,
            @PathVariable int margealtitude
            ) throws GestionException{
        
        List<Publication> liste = new ArrayList<Publication>();
        HttpStatus status = null;
            
        try {
            liste = publicationService.recommendation(sujet, date1, date2, heure1, heure2, niveaujardin, altitude, margealtitude, ville);
            status = HttpStatus.OK;
            return new ResponseEntity<>(liste, status);
        } catch (Exception e) {
            GestionException ex =  new GestionException(HttpStatus.BAD_REQUEST,e.getMessage());
            throw ex;
        }
    }
    
    
}
