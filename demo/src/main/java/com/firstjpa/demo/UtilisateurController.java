/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo;

import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Utilisateur;
import com.firstjpa.demo.model.Login;
import com.firstjpa.demo.model.Token;
import com.firstjpa.demo.service.UtilisateurService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author
 */
@RestController
public class UtilisateurController {
    @Autowired
    UtilisateurService utilisateurService;
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<Token> login(@RequestBody Login u) throws GestionException{
        Token token = null;
        ResponseEntity<Token> response = null;
        try {
            token = utilisateurService.login(u);
            response = new ResponseEntity<Token>(token, HttpStatus.OK);
        } catch (GestionException e) {
            throw e;
        }
        return response;
    }

}
