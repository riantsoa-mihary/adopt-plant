/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.utilitaire.Constante;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author
 */
@Service
public class FilesStorageServiceImpl implements FilesStorageService{

    private final Path root = Paths.get(Constante.UPLOAD_DIR);
    public FilesStorageServiceImpl(){
        init();
    }
    @Override
    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            //throw new RuntimeException(MessageException.FOLDER_NOT_CREATED);
        }
    }

    private String getNewFileName(MultipartFile multipartFile) {
        String newName = multipartFile.getOriginalFilename();
        Date d = new Date();
        long l = d.getTime();
        String time = String.valueOf(l);
        newName = time+newName;        
        return newName;
    }
            
    @Override
    public String save(MultipartFile multipartFile) throws Exception{
        String newName = "";
        try {
            newName = this.getNewFileName(multipartFile);
            Files.copy(multipartFile.getInputStream(), this.root.resolve(newName));
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Could not store the file. Error : "+e.getMessage());
        }
        return newName;
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()){
                return resource;
            }else{
                throw new RuntimeException(" Could not read file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error :"+e.getMessage());
        }
    }
    
}
