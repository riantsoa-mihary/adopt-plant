/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author
 */
public interface FilesStorageService {
    public void init();
    public String save(MultipartFile multipartFile) throws Exception;
    public Resource load(String filename);
    
}
