/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.dao.PublicationDao;
import com.firstjpa.demo.dao.PublicationRepository;
import com.firstjpa.demo.model.ControlPublicationInsert;
import com.firstjpa.demo.utilitaire.Constante;
import com.firstjpa.demo.model.Publication;
import com.firstjpa.demo.model.Recommendation;
import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Token;
import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author
 */
@Service
public class PublicationServiceImpl implements PublicationService{
    
    private PublicationRepository publicationRepository;
    private PublicationDao publicationDao;
    
    private TokenService tokenService;

    @Autowired
    public PublicationServiceImpl(
            PublicationRepository publicationRepository,
            PublicationDao publicationDao,
            TokenService tokenService
            ) {
        this.publicationRepository = publicationRepository;
        this.publicationDao = publicationDao;
        this.tokenService = tokenService;
    }

    @Override
    public Publication createPublication(Publication p, String token) throws GestionException{
        // Controle valeur avant l'acces base
        ControlPublicationInsert control = new ControlPublicationInsert(p);
        if(control.hasException()){
            throw control.getException();
        }
        p = control.getPublication(); 
        // Controle du token ( valeur et Validiter (base))
        // Exception si token : vide / expirer / innexistant
        Token tok = tokenService.controlToken(token);
        // tok.getIdUtilisateur() est NON vide : 
        // parceque c'est une valeur recuperer depuis la base
        p.setIdutilisateur(tok.getIdutilisateur());
        p.setEtat(Constante.STATE_CREATED);
        Publication result =  publicationRepository.save(p);
        return result;
    }


    @Override
    public List<Publication> recommendation(String sujet, String date1, String date2, double heure1, double heure2, int niveaujardin, double altitude, int margeAltitude, String ville) throws ParseException{
        Recommendation recommandation = new Recommendation(sujet, date1, date2, heure1, heure2, niveaujardin, altitude, margeAltitude, ville);
        List<Publication> liste = recommandation.getRecommendation(publicationDao) ;
        return liste;
    }
}
