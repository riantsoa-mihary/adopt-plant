/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.dao.TokenRepository;
import com.firstjpa.demo.dao.UtilisateurRepository;
import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Token;
import com.firstjpa.demo.utilitaire.Constante;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author
 */
@Service
public class TokenServiceImpl implements TokenService{
    
    TokenRepository tokenRepository;
    
    @Autowired
    public TokenServiceImpl(
            TokenRepository tokenRepository
    ){
        this.tokenRepository = tokenRepository;
    }
    /**
     * Controle Du Token : valeur simple, et validiter dans la Base de donnee
     * @param token
     * @return
     * @throws GestionException 
     */
    public Token controlToken(String token) throws GestionException{
        
        // Controle de valeur pour token (avant d'acceder a la base de donnee)
        Token control = new Token();
        control.setToken(token);
                
        // Controle et Recupertation du token depuis la base
        Token result = null;
        List<Token> list = this.tokenRepository.findByToken(token);
        if(list == null || list.size() <= 0){
            GestionException exception = new GestionException(HttpStatus.BAD_REQUEST);
            exception.add("token", Constante.TOKEN_VIDE_MESSAGE);
            throw exception;
        }else if(list != null && list.size() > 0){
            result = (Token)list.get(0);
        }
        return result;
    }
}
