/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Token;

/**
 *
 * @author
 */
public interface TokenService {
    Token controlToken(String token) throws GestionException;
}
