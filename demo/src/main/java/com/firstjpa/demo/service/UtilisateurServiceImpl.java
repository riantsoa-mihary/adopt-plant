/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.dao.TokenRepository;
import com.firstjpa.demo.dao.UtilisateurRepository;
import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Utilisateur;
import com.firstjpa.demo.model.Login;
import com.firstjpa.demo.model.Token;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author
 */
@Service
public class UtilisateurServiceImpl implements UtilisateurService{

    UtilisateurRepository utilisateurRepository;
    TokenRepository tokenRepository;
    
    
    @Autowired
    public UtilisateurServiceImpl(
            UtilisateurRepository utilisateurRepository,
            TokenRepository tokenRepository
    ){
        this.utilisateurRepository = utilisateurRepository;
        this.tokenRepository = tokenRepository;
    }
            
    @Override 
    public Token login(Login u) throws GestionException{
        Token result = null;
        Utilisateur utilisateur = null;
        u.controlValeurLogin();
        List<Utilisateur> list = this.utilisateurRepository.findByLogin(u.getEmail(), u.getMdp());
        if(list == null || list.size() <= 0) {
            throw new GestionException(HttpStatus.BAD_REQUEST, "Utilisateur introuvable");
        }else if (list != null && list.size() > 0){
            utilisateur = (Utilisateur)list.get(0);
            result = tokenRepository.save(new Token(utilisateur.getId()));
            result.setUtilisateur(utilisateur);
        }
        return result;
    }
    
    
}
