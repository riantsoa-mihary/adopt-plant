/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo.service;

import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.model.Publication;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author
 */
public interface PublicationService {
    List<Publication> recommendation(String sujet, String date1, String date2, double heure1, double heure2, int niveaujardin, double altitude, int margeAltitude, String ville) throws ParseException;
    Publication createPublication(Publication p, String token) throws GestionException;
}
