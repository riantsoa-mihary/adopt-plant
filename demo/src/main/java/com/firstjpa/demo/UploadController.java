/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.firstjpa.demo;

import com.firstjpa.demo.model.GestionException;
import com.firstjpa.demo.service.FilesStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author
 */
@RestController
public class UploadController {
    
    @Autowired
    FilesStorageService filesStorageService;
    
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) throws GestionException{
            String newName = "";
            try {
                newName = filesStorageService.save(file);
            } catch (Exception e) {
                GestionException ex = new GestionException(HttpStatus.BAD_REQUEST, e.getMessage());
                throw ex;
            }
        return new ResponseEntity<String>(newName, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/load", method = RequestMethod.GET)
    public ResponseEntity<Resource> load(@RequestParam("file") String filename){
            Resource resource =  filesStorageService.load(filename);
        return new ResponseEntity<Resource>(resource, HttpStatus.FOUND);
    }
}
