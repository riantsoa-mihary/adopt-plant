/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editoe.
 */
/**
 * Author:  riantsoa
 * Created: 12 nov. 2021
 */

create view pubOffreComplet as
select
	p.id,
	p.idutilisateur,
	p.datepub,
	p.sujet,
	p.descsujet,
	p.nombre,
	p.etat,
	p.image,
	p.idendroit,
	p.daterdv,
	p.heurerdv,
	p.altitude,
	p.niveaujardin,
	e.idville,
	e.endroit,
	e.descendroit,
	e.latitude,
	e.longitude,
	v.nom as nomville,
	u.nom as nomutilisateur,
	u.photos as photosutilisateur
from
	puboffre p
	join endroit e on p.idendroit = e.id 
	join ville v on e.idville = v.id 
	join utilisateur u on p.idutilisateur = u.id ;
	