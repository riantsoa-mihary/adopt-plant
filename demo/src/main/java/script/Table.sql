

create role plant;
create database plant owner plant ;
alter user plant with encrypted password 'plant';
alter role plant with login;

-- DROP TABLE public.utilisateur;

CREATE TABLE public.utilisateur (
	id text NOT NULL default 'US'||nextval('seq_utilisateur'),
	nom text NOT NULL,
        email text NOT NULL,
	mdp text NOT NULL,
	photos text NULL,
	niveaujardin int4 NULL,
	superficiejardin numeric(7,2) NULL DEFAULT 1,
	CONSTRAINT utilisateur_pkey PRIMARY KEY (id)
);

-- DROP TABLE public.image;

CREATE TABLE public.image (
	id text NOT NULL,
	nom text NULL DEFAULT ''::text,
	link text NOT NULL,
	idutilisateur text NOT NULL,
	etat int4 NULL,
	CONSTRAINT image_pkey PRIMARY KEY (id),
	CONSTRAINT utilisateur_idimage_fkey FOREIGN KEY (idutilisateur) REFERENCES public.utilisateur(id)
);


create table PubDemande(
	id text not null primary key,
	idUtilisateur text not null,
	datePub date default now(),
	sujet text not null,
	descSujet text default '' ,
	nombre integer not null default 1,
	etat integer not null,
	foreign key (idUtilisateur) references Utilisateur(id)
);

create table Ville(
	id text not null primary key,
	nom text not null
);

-- DROP TABLE public.endroit;

CREATE TABLE public.endroit (
	id text NOT NULL,
	idville text NOT NULL,
	endroit text NOT NULL,
	descendroit text NULL DEFAULT ''::text,
	latitude decimal NULL,
	longitude decimal NULL,
	CONSTRAINT endroit_pkey PRIMARY KEY (id),
	CONSTRAINT endroit_idville_fkey FOREIGN KEY (idville) REFERENCES public.ville(id)
);
-- Drop table

-- DROP TABLE public.puboffre;

CREATE SEQUENCE seq_puboffre ;
CREATE TABLE public.puboffre (
	id text NOT NULL default 'PO'||nextval('seq_puboffre'),
	idutilisateur text NOT NULL,
	datepub date NOT NULL DEFAULT now(),
	sujet text NOT NULL,
	descsujet text NULL DEFAULT ''::text,
	nombre int4 NOT NULL DEFAULT 1,
	etat int4 NOT NULL DEFAULT 1, 
	image text NULL,
	idendroit text NOT NULL,
	daterdv date NOT NULL DEFAULT now(),
	heurerdv numeric(7,2) NOT NULL,
	altitude numeric(7,2) NOT NULL DEFAULT 0,
        niveaujardin integer NOT NULL default 1,
	CONSTRAINT puboffre_pkey PRIMARY KEY (id),
	CONSTRAINT puboffre_idendroit_fkey FOREIGN KEY (idendroit) REFERENCES public.endroit(id),
	CONSTRAINT puboffre_idutilisateur_fkey FOREIGN KEY (idutilisateur) REFERENCES public.utilisateur(id)
);
/*
    etat PubOffre :
    0 : annuler
    1-10 : cree
*/

create table reservation(
	id text not null primary key,
	idPubOffre text not null,
	idUtilisateur text not null,
	nombre integer not null default 1,
	etat integer not null,
	foreign key (idPubOffre) references PubOffre(id),
	foreign key (idUtilisateur) references Utilisateur(id)
);
/*
    etat reservation :
    0 : annuler
    1-10 : cree
*/


create table Proposition(
	id text not null primary key,
	idUtilisateur text not null, 
	idPubDemande text not null,
	dateProp timestamp default now(),
	description text default '',
	nombre integer not null default 1,
	image text,
	idEndroit text not null,
	dateRdv date not null,
	heureRdv decimal(7,2) not null,
	etat integer,
	foreign key (idUtilisateur) references Utilisateur(id),
	foreign key (idPubDemande) references PubDemande(id),
	foreign key (idEndroit) references Endroit(id)
);


-- Token 


create table token (
    idutilisateur text not null,
    expiration timestamp not null default now() + '1d', -- 1 jours de validiter
    token text not null default  md5(''||now()),
    foreign key (idutilisateur) references Utilisateur(id)
);


--

create view tokenvalide as
select
	*
from
	token
where
	expiration >= now() ;

-----