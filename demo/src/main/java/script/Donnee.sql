/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  riantsoa
 * Created: 12 nov. 2021
 */



-- Auto-generated SQL script #202111160940
INSERT INTO public.utilisateur (id,nom,email,mdp,photos,niveaujardin)
	VALUES ('UT0001','Vao','vao@gmail.com','19854f060062f1e0f511cdd691a4bdd5','vao.jpg',1);
INSERT INTO public.utilisateur (id,nom,email,mdp,photos,niveaujardin,superficiejardin)
	VALUES ('UT0002','Tina','tina@gmail.com','ef2afe0ea76c76b6b4b1ee92864c4d5c','tina.jgp',11,5);

INSERT INTO public.ville (id,nom) VALUES
	 ('V001','Antananarivo'),
	 ('V002','Toamasina');

-- Auto-generated SQL script #202111160945
INSERT INTO public.endroit (id,idville,endroit,descendroit)
	VALUES ('END001','V001','Analakely','Eo alohan''ny Gare');
INSERT INTO public.endroit (id,idville,endroit,descendroit,latitude,longitude)
	VALUES ('END002','V001','Ankaraobato','Simad',-18.9692285,47.533642);

INSERT INTO public.puboffre (id,idutilisateur,datepub,sujet,descsujet,nombre,etat,image,idendroit,daterdv,heurerdv,altitude) VALUES
	 ('PO0001','UT0001','2021-11-12','Rose','Rose magnifique',5,1,'rose.jpeg','END001','2021-11-12',57600.00,1271.00),
	 ('PO0002','UT0001','2021-11-12','Rose','Rose rouge',5,0,'rose.jpeg','END001','2021-11-20',57600.00,1271.00);


INSERT INTO public.reservation (id,idpuboffre,idutilisateur,nombre,etat)
	VALUES ('RES0001','PO0001','UT0002',2,1);
INSERT INTO public.reservation (id,idpuboffre,idutilisateur,nombre,etat)
	VALUES ('RES0002','PO0001','UT0002',2,0);


---



INSERT INTO public.puboffre (idutilisateur,datepub,sujet,descsujet,nombre,etat,image,idendroit,daterdv,heurerdv,altitude,niveaujardin) VALUES
	 ('UT0001','2021-12-13','Rose','Rose rouge',5,0,'1638964886295argon.png','END001','2021-11-20',57600.00,1280.00,1),
	 ('UT0001','2021-12-15','Rose','Rose magnifique',5,1,'1638964886295argon.png','END001','2021-11-12',57600.00,1271.00,1),
	 ('UT0001','2021-12-12','Rose','Rose rouge j haut',5,0,'1638964886295argon.png','END001','2021-11-20',57600.00,1756.00,1),
	 ('UT0001','2021-12-12','Rose','Rose rouge bas',5,0,'1638964886295argon.png','END001','2021-11-20',57600.00,1271.00,1),
	 ('UT0001','2021-12-12','Rose','Rose rouge null',5,0,'1638964886295argon.png','END001','2021-11-20',57600.00,0.00,1),
	 ('UT0001','2021-12-12','Rose','Rose rouge',5,0,'1638964886295argon.png','END001','2022-11-20',57600.00,2100.00,1),
	 ('UT0001','2021-12-12','Rose','Rose rouge j bas',5,0,'1638964886295argon.png','END001','2022-11-20',57600.00,1756.00,1),
	 ('UT0001','2021-12-02','Rose','Rose rouge j bas',5,1,'1638964886295argon.png','END001','2021-12-11',0.00,5.00,1),
	 ('UT0001','2021-12-11','Rose','Rose rouge j bas',5,1,'1638964886295argon.png','END001','2021-12-11',0.00,5.00,1),
	 ('UT0001','2021-12-08','rouge','men',2,1,'1638991287374Sélection_009.png','END001','2021-12-24',62580.00,1000.00,11);
INSERT INTO public.puboffre (idutilisateur,datepub,sujet,descsujet,nombre,etat,image,idendroit,daterdv,heurerdv,altitude,niveaujardin) VALUES
	 ('UT0001','2021-12-08','rouge','men',2,1,'1638991338760Sélection_009.png','END001','2021-12-24',62580.00,0.00,11),
	 ('UT0001','2021-12-08','rouge','men',2,1,'  ','END001','2021-12-24',62580.00,0.00,11),
	 ('UT0001','2021-12-10','rose','Rose rouge j bas',5,1,'  ','END001','2021-12-11',0.00,5.00,1),
	 ('UT0001','2021-12-10','rose','Rose rouge j bas',5,1,'  ','END001','2021-12-11',0.00,5.00,1);